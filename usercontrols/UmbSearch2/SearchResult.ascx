﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchResult.ascx.cs"
    Inherits="UmbSearch2.UserControls.SearchResult" %>
<style type="text/css">
    ul
    {
        margin: 0;
        padding: 0;
        list-style-type: none;
    }
    li
    {
        margin-bottom: 10px;
    }
</style>
<h3><asp:Literal ID="ltlMessage" runat="server"></asp:Literal></h3>
<asp:PlaceHolder ID="PlaceHolderPagingTop" runat="server" Visible="false"></asp:PlaceHolder>
<ul>
    <asp:PlaceHolder ID="plhSearchResults" runat="server"></asp:PlaceHolder>
</ul>
<asp:PlaceHolder ID="PlaceHolderPagingBottum" runat="server" Visible="false"></asp:PlaceHolder>
