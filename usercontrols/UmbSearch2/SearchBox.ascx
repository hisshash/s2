﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchBox.ascx.cs" Inherits="UmbSearch2.UserControls.SearchBox" %>
<asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch" class="searchbox>
    <asp:TextBox ID="txtInput" runat="server" class="searchterms"></asp:TextBox>&nbsp;
    <asp:Button ID="btnSearch" runat="server" 
        Text="<%$ Resources:UmbSearch2Resources, Search %>" OnClick="btnSearch_Click" class="searchbutton"/><br />
</asp:Panel>
