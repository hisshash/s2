﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReindexSite.aspx.cs" Inherits="UmbSearch2.ReindexSite" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Untitled Page</title>
</head>
<body>
	<form id="form1" runat="server">
	<div>		
		<asp:ScriptManager ID="ScriptManager1" runat="server">
		</asp:ScriptManager>
		<asp:UpdatePanel ID="UpdatePanel1" runat="server">
			<ContentTemplate>
				Reindex the website
				<asp:Button ID="btnReindex" runat="server" Text="Start" OnClick="btnReindex_Click" />
				<br />
				<br />
				<asp:Label ID="lblResult" runat="server" Text="" Visible="false"></asp:Label>
			</ContentTemplate>
		</asp:UpdatePanel>
		<asp:UpdateProgress runat="server" ID="updateProgress1">
			<ProgressTemplate>
				<div>
					Busy reindexing ...
					<img src="/media/wait.gif" alt="Wait" />
					<input id="Button1" onclick="AbortPostBack()" type="button" value="Cancel" style="font-size: xx-small;" />
				</div>
			</ProgressTemplate>
		</asp:UpdateProgress>
	</div>
	</form>

	<script type="text/javascript">
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_initializeRequest(InitializeRequest);

    function InitializeRequest(sender, args) {
        if (prm.get_isInAsyncPostBack())
        {
            args.set_cancel(true);
        }
    }

    function AbortPostBack() {
        if (prm.get_isInAsyncPostBack()) {
         prm.abortPostBack();
        }        
    }
	</script>

</body>
</html>
