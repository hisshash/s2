﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchResultTemplate.ascx.cs" Inherits="UmbSearch2.UserControls.SearchResultTemplate" %>
<asp:HyperLink ID="HyperLink1" runat="server"></asp:HyperLink><br />
<asp:Literal ID="ltlSummary" runat="server"></asp:Literal><br />
<div style="font-size: small" visible="true">
	(<%= Resources.UmbSearch2Resources.Match%>
	<asp:Literal ID="ltlMatch" runat="server"></asp:Literal>)
</div>
