﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pager.ascx.cs" Inherits="UmbSearch2.UserControls.Pager" %>
<asp:HyperLink ID="hplPrevious" runat="server" style="text-decoration:none">&lt; <%= Resources.UmbSearch2Resources.Previous%> |</asp:HyperLink>
<asp:PlaceHolder ID="plhPaging" runat="server"></asp:PlaceHolder>
<asp:HyperLink ID="hplNext" runat="server" style="text-decoration:none">| <%= Resources.UmbSearch2Resources.Next%> &gt;</asp:HyperLink>