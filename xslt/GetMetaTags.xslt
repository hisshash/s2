<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets ">

<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">
<title><xsl:for-each select="$currentPage/ancestor-or-self::node[not(string(./data [@alias = 'title'])='')]">
	<xsl:sort select="position()" data-type="number" order="descending"/>
	<xsl:if test="position()=1">
		<xsl:value-of select="./data [@alias = 'title']" />
	</xsl:if>
	</xsl:for-each></title>
<meta name="keywords">
	<xsl:attribute name="content">
		<xsl:for-each select="$currentPage/ancestor-or-self::node[not(string(./data [@alias = 'keywords'])='')]">
			<xsl:sort select="position()" data-type="number" order="descending"/>	
			<xsl:if test="position()=1">
				<xsl:value-of select="./data [@alias = 'keywords']" />
			</xsl:if>
		</xsl:for-each>
	</xsl:attribute>
</meta>
<meta name="description">
	<xsl:attribute name="content">
		<xsl:for-each select="$currentPage/ancestor-or-self::node[not(string(./data [@alias = 'description'])='')]">
			<xsl:sort select="position()" data-type="number" order="descending"/>	
			<xsl:if test="position()=1">
				<xsl:value-of select="./data [@alias = 'description']" />
			</xsl:if>
		</xsl:for-each>
	</xsl:attribute>
</meta>
</xsl:template>
</xsl:stylesheet>