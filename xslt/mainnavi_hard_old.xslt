<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">

<!-- start writing XSLT -->

<div id="divfornavi">
<!---START search hideload here-->
<div align="right" style="width:100%;height:auto;float:right">
<a id="displaySearch" href="javascript:toggle();" onmouseover="MM_swapImage('Image9','','/images/search_btnroll.jpg',1)" onmouseout="MM_swapImgRestore()">
        <img src="/images/search_btn.jpg" name="Image9" width="41" height="40" border="0" id="Image9" />      
            </a>
</div>
<div id="divsearchnavi" style="display: none;float:right">
<div style="margin:7px 0 0 45px;float:left">
<form action="/search.aspx" method="post">
<table width="200" border="0">
  <tr>
    <th scope="col"><input type="text" name="search" size="28" /></th>
    <th scope="col"><input type="image" name="find" src="/images/findbtn.png" width="28" height="23" /></th>
  </tr>
</table>
</form></div>
 </div>         
 <!---search hideload here END-->
<xsl:variable name ="newContact" select ="umbraco.library:GetXmlNodeById(1079)"/>
<xsl:variable name ="newProducts" select ="umbraco.library:GetXmlNodeById(1065)"/>
<xsl:variable name ="newApplications" select ="umbraco.library:GetXmlNodeById(1066)"/>
<xsl:variable name ="newDome" select ="umbraco.library:GetXmlNodeById(1073)"/>
<xsl:variable name ="newArch" select ="umbraco.library:GetXmlNodeById(1074)"/>
<xsl:variable name ="newFloor" select ="umbraco.library:GetXmlNodeById(1075)"/>
<xsl:variable name ="newAbout" select ="umbraco.library:GetXmlNodeById(1057)"/>
<xsl:variable name ="newHistory" select ="umbraco.library:GetXmlNodeById(1071)"/>
<xsl:variable name ="newIP" select ="umbraco.library:GetXmlNodeById(1059)"/>
<xsl:variable name ="newProjects" select ="umbraco.library:GetXmlNodeById(1072)"/>
<xsl:variable name ="newLicensees" select ="umbraco.library:GetXmlNodeById(1076)"/>
<xsl:variable name ="newHome" select ="umbraco.library:GetXmlNodeById(1046)"/>


		
	<ul id="divbuttonnavi">
	<li><a href="{umbraco.library:NiceUrl(1079)}">
   <xsl:value-of select="$newContact/data [@alias='idTitlePageSub']"/>   
        </a>
            
        </li>

  <li><a href="{umbraco.library:NiceUrl(1065)}" onmouseover="mopen('m2')" onmouseout="mclosetime()">
      <xsl:value-of select="$newProducts/data [@alias='idTitlePageSub']"/> 
        </a>
            <div id="m2" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
            <a href="{umbraco.library:NiceUrl(1066)}"><xsl:value-of select="$newApplications/data [@alias='idTitlePageSub']"/> </a>
            <a href="{umbraco.library:NiceUrl(1073)}"><xsl:value-of select="$newDome/data [@alias='idTitlePageSub']"/> </a>
            <a href="{umbraco.library:NiceUrl(1074)}"><xsl:value-of select="$newArch/data [@alias='idTitlePageSub']"/> </a>
            <a href="{umbraco.library:NiceUrl(1075)}"><xsl:value-of select="$newFloor/data [@alias='idTitlePageSub']"/></a>
            </div>
	
        </li>

 <li><a href="{umbraco.library:NiceUrl(1057)}" onmouseover="mopen('m1')" onmouseout="mclosetime()">
      <xsl:value-of select="$newAbout/data [@alias='idTitlePageSub']"/> 
        </a>
            <div id="m1" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
            <a href="{umbraco.library:NiceUrl(1071)}"><xsl:value-of select="$newHistory/data [@alias='idTitlePageSub']"/></a>
            <a href="{umbraco.library:NiceUrl(1059)}"><xsl:value-of select="$newIP/data [@alias='idTitlePageSub']"/></a>
            <a href="{umbraco.library:NiceUrl(1072)}"><xsl:value-of select="$newProjects/data [@alias='idTitlePageSub']"/></a>
	    <a href="{umbraco.library:NiceUrl(1076)}"><xsl:value-of select="$newLicensees/data [@alias='idTitlePageSub']"/></a>
            </div>
        </li>

	 <li><a href="{umbraco.library:NiceUrl(1046)}">
        <xsl:value-of select="$newHome/@nodeName"/>  
        </a> 
        </li>

       
      
        
	</ul> 
            
            
            
            
</div>



</xsl:template>

</xsl:stylesheet>