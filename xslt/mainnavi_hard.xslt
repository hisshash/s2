<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">

<!-- start writing XSLT -->

<div id="divfornavi">
<!---START search hideload here-->
<div align="right" style="width:100%;height:auto;float:right">
<a id="displaySearch" href="javascript:toggle();" onmouseover="MM_swapImage('Image9','','/images/search_btnroll.jpg',1)" onmouseout="MM_swapImgRestore()">
        <img src="/images/search_btn.jpg" name="Image9" width="41" height="40" border="0" id="Image9" />      
            </a>
</div>
<div id="divsearchnavi" style="display: none;float:right">
<div style="margin:7px 0 0 45px;float:left">
<form action="/search.aspx" method="post">
<table width="200" border="0">
  <tr>
    <th scope="col"><input type="text" name="search" size="28" /></th>
    <th scope="col"><input type="image" name="find" src="/images/findbtn.png" width="28" height="23" /></th>
  </tr>
</table>
</form></div>
 </div>         
 <!---search hideload here END-->

		
	<ul id="divbuttonnavi">
<xsl:variable name="absoluteRoot" select="umbraco.library:GetXmlAll()"/>


<xsl:for-each select="$absoluteRoot/node/node">
<xsl:if test="./@id != 1078">
<xsl:if test="./@id != 1099">
<xsl:if test="./@id != 1101">
		<xsl:variable name="NaviNode" select="@nodeName" />
<li>  
		<a href="{umbraco.library:NiceUrl(@id)}" onmouseover="mopen('{concat('m',position())}')" onmouseout="mclosetime()">
			<xsl:value-of select="./data[@alias='navTitle']"/>
        	</a>
		<xsl:if test="count(./node) !=0">
			<!--media count-->
			<!--Start Div-->
<div name="mnav" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
<!--assigning those id with this simple line below-->
<xsl:attribute name="id"><xsl:value-of select="concat('m',position())"/></xsl:attribute>
		<xsl:for-each select="./node">
			<a href="{umbraco.library:NiceUrl(@id)}"><xsl:value-of select="./data[@alias='navTitle']" disable-output-escaping="yes"/></a>
		</xsl:for-each>
</div>
		</xsl:if>			
</li>
	</xsl:if>
	</xsl:if>
	</xsl:if>

</xsl:for-each>

       
      
        
	</ul> 
            
            
            
            
</div>



</xsl:template>

</xsl:stylesheet>
