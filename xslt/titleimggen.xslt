<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">

<!-- start writing XSLT -->


  <xsl:variable name="imgUrl" select="$currentPage/data [@alias='idTitlePage']"/>
    <xsl:if test="$imgUrl != ''">        
        <img>
            <xsl:attribute name="src">
                <xsl:text>/umbraco/ImageGen.ashx?text=</xsl:text>
                <xsl:value-of select="$imgUrl" disable-output-escaping="yes"/>   
                <xsl:text>&amp;font=/font/HelveticaNeueLTStd-UltLt.ttf</xsl:text>
		<xsl:text>&amp;fontstyle= bold </xsl:text>
		<xsl:text>&amp;fontsize=42</xsl:text>
		<xsl:text>&amp;fontcolor=0070ba</xsl:text>
                <xsl:text>&amp;width=</xsl:text>
            </xsl:attribute>
        </img>
   </xsl:if>

<xsl:variable name="imgUrla" select="$currentPage/data [@alias='idTitlePageSub']"/>
    <xsl:if test="$imgUrla != ''">        
        <img>
            <xsl:attribute name="src">
                <xsl:text>/umbraco/ImageGen.ashx?text=</xsl:text>
                <xsl:value-of select="$imgUrla" disable-output-escaping="yes"/>    
                <xsl:text>&amp;font=/font/HelveticaNeueLTStd-UltLt.ttf</xsl:text>
		<xsl:text>&amp;fontstyle= bold </xsl:text>
		<xsl:text>&amp;fontsize=42</xsl:text>
		<xsl:text>&amp;fontcolor=0070ba</xsl:text>
                <xsl:text>&amp;width=</xsl:text>
            </xsl:attribute>
        </img>
   </xsl:if>



        <xsl:variable name="mediaTitle" select="number($currentPage/data[@alias='TitleImage'])" />  
    	<xsl:if test="$mediaTitle &gt; 0">  
         	<xsl:variable name="mediaTitlenode" select="umbraco.library:GetMedia($mediaTitle, 0)" />  
         		<xsl:if test="count($mediaTitlenode/data) &gt; 0 and string($mediaTitlenode/data[@alias='umbracoFile']) != ''">  
			<img src="{$mediaTitlenode/data[@alias='umbracoFile']}" alt="[image]" height="{$mediaTitlenode/data[@alias='umbracoHeight']}" width="{$mediaTitlenode/data[@alias='umbracoWidth']}" />
     			</xsl:if>  
	</xsl:if>






</xsl:template>

</xsl:stylesheet>