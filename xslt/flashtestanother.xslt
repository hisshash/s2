<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:Stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxml="urn:schemas-microsoft-com:xslt"
    xmlns:umbraco.library="urn:umbraco.library"
    exclude-result-prefixes="msxml umbraco.library">


<xsl:output method="html" omit-xml-declaration="yes"/>


<xsl:param name="currentPage"/>
<xsl:variable name="videonode" select="/macro/SettingsNode"/>
<xsl:variable name="videoid" select="umbraco.library:Replace(/macro/videoid,' ','')"/>
<xsl:variable name="videopath" select="umbraco.library:GetMedia($currentPage/data [@alias='tFlashFile'], 'false')/data [@alias = 'umbracoFile']"/>
<xsl:variable name="videoheight" select="$currentPage/data[@alias='tFlashHeight']"/>
<xsl:variable name="videowidth" select="$currentPage/data[@alias='tFlashWidth']"/>



<!-- ============================================================= -->

<xsl:template match="/">

<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="fpdownload.macromedia.com/.../swflash.cab,0,0,0" width="{$videowidth}" height="{$videoheight}">
        <param name="movie" value="{$videopath}" />
  <param name="quality" value="high" />
  <param name="allowScriptAccess" value="always" />
        <param name="wmode" value="transparent"/>
     <embed src="{$videopath}"
      quality="high"
      type="application/x-shockwave-flash"
      WMODE="transparent"
      width="{$videowidth}"
      height="{$videoheight}"
      pluginspage="http://www.macromedia.com/go/getflashplayer"
      allowScriptAccess="always" />
</object>	
</xsl:template>
<!-- ============================================================= -->

</xsl:stylesheet>