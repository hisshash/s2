<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:Stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxml="urn:schemas-microsoft-com:xslt"
    xmlns:umbraco.library="urn:umbraco.library"
    exclude-result-prefixes="msxml umbraco.library">


<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
<xsl:variable name="videonode" select="/macro/SettingsNode"/>
<xsl:variable name="videoid" select="umbraco.library:Replace(/macro/videoid,' ','')"/>
<xsl:variable name="videopath" select="$videonode/node/data[@alias='umbracoFile']"/>
<xsl:variable name="videoheight" select="/macro/VideoHeight"/>
<xsl:variable name="videowidth" select="/macro/VideoWidth"/>



<!-- ============================================================= -->

<xsl:template match="/">
	
<div>
	<xsl:attribute name="id">
		<xsl:value-of select="$videoid" />
	</xsl:attribute>
	<p><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this content.</p>
</div>
<script type="text/javascript" src="/scripts/swfobject_22.js"></script>
<script type="text/javascript">
<![CDATA[

var path = "]]><xsl:value-of select="$videopath" /><![CDATA[";
var videowidth = "]]><xsl:value-of select="$videowidth" /><![CDATA[";
var videoid = "]]><xsl:value-of select="$videoid" /><![CDATA[";
var videoheight = "]]><xsl:value-of select="$videoheight" /><![CDATA[";
var flashvars = {};
var attributes = {};
var params = {
  wmode: "transparent",
  allowfullscreen: "true",
  allscriptaccess: "always"
};

swfobject.embedSWF(path, videoid, videowidth, videoheight, "9.0.0", flashvars, params, attributes);

]]>
</script>


</xsl:template>
<!-- ============================================================= -->

</xsl:stylesheet>