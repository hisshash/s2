<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>

<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>



<xsl:template match="/">

<!-- start writing XSLT -->




<!-- renderdummyimage -->

<xsl:variable name="mediaDum" select="number($currentPage/node/data[@alias='idImageGallery'])" />  
    	<xsl:if test="$mediaDum &gt; 0">  
         	<xsl:variable name="mediaDumnode" select="umbraco.library:GetMedia($mediaDum, 0)" />  
         		<xsl:if test="count($mediaDumnode/data) &gt; 0 and string($mediaDumnode/data[@alias='umbracoFile']) != ''">  
<div id="Dum" style="width:882px;height:466px;background-color:#555555;float:left;">
<img src="{$mediaDumnode/data[@alias='umbracoFile']}" alt="[image]" height="{$mediaDumnode/data[@alias='umbracoHeight']}" width="{$mediaDumnode/data[@alias='umbracoWidth']}" />
</div>
     			</xsl:if> 
 	</xsl:if>



<xsl:for-each select="$currentPage/node[string(data [@alias='umbracoNaviHide']) != '1']">
<xsl:if test="data[@alias='idImageGallery'] != ''">

<!-- renderrealgallery -->

<xsl:variable name="mediaTitle" select="number(./data[@alias='idImageGallery'])" />  
    	<xsl:if test="$mediaTitle &gt; 0">  
         	<xsl:variable name="mediaTitlenode" select="umbraco.library:GetMedia($mediaTitle, 0)" />  
         		<xsl:if test="count($mediaTitlenode/data) &gt; 0 and string($mediaTitlenode/data[@alias='umbracoFile']) != ''">  
<div name="imgjq" style="display:none; width:882px;height:466px;background-color:#555555;float:left;">
<xsl:attribute name="id"><xsl:value-of select="concat('imgjq',position())"/></xsl:attribute>
<img src="{$mediaTitlenode/data[@alias='umbracoFile']}" alt="[image]" height="{$mediaTitlenode/data[@alias='umbracoHeight']}" width="{$mediaTitlenode/data[@alias='umbracoWidth']}" >
</img>
</div>
     			</xsl:if> 
 	</xsl:if>
	</xsl:if>

<!--if FLASH-->
<xsl:if test="data[@alias='FlashFiles'] != ''">

<xsl:variable name="mediaVideo" select="number(./data[@alias='FlashFiles'])" />  
    	<xsl:if test="$mediaVideo &gt; 0">  
         	<xsl:variable name="mediaVideonode" select="umbraco.library:GetMedia($mediaVideo, 0)" />  
         		<xsl:if test="count($mediaVideonode/data) &gt; 0 and string($mediaVideonode/data[@alias='umbracoFile']) != ''">  
<div name="imgjq" style="display:none; width:882px;height:466px;background-color:#555555;float:left;">
<xsl:attribute name="id"><xsl:value-of select="concat('imgjq',position())"/></xsl:attribute>
<!---OBJECT HERE-->
<xsl:variable name="videoheight" select="./data[@alias='FlashHeight']"/>
<xsl:variable name="videowidth" select="./data[@alias='FlashWidth']"/>
	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="fpdownload.macromedia.com/.../swflash.cab,0,0,0" width="882" height="466">
        <param name="movie" value="{$mediaVideonode/data[@alias='umbracoFile']}" />
  <param name="quality" value="high" />
  <param name="allowScriptAccess" value="always" />
       
     <embed src="{$mediaVideonode/data[@alias='umbracoFile']}"
      quality="high"
      type="application/x-shockwave-flash"
      
      width="882"
      height="466"
      pluginspage="http://www.macromedia.com/go/getflashplayer"
      allowScriptAccess="always" />
</object>
<!-- ============================================================= -->
</div>

     			</xsl:if> 
 	</xsl:if>

</xsl:if>
	


<!--if FLASH END-->
</xsl:for-each>
	

<div id="divthumbnailcontainer">

<xsl:for-each select="$currentPage/node[string(data [@alias='umbracoNaviHide']) != '1']">
<xsl:if test="data[@alias='idImageGallery'] != ''">
 
 <xsl:variable name="media" select="umbraco.library:GetMedia(number(./data[@alias='idImageGallery']), 0)" />                                                      
  <xsl:if test="count($media/data[@alias='umbracoFile']) &gt; 0">
 <a href="javascript:showonly('{concat('imgjq',position())}')">
<div id="divthumbnail">
<img class="ExcommKnap" src="{$media/data[@alias='umbracoFile']} " width = "120px" height ="63px" >
</img>
</div>
   </a>
  </xsl:if>
 </xsl:if>

<!--IF flash thumbnail-->
<xsl:if test="data[@alias='FlashFiles'] != ''">
 <xsl:variable name="mediaflsh" select="umbraco.library:GetMedia(number(./data[@alias='FlashThumbnail']), 0)" />                                                      
  <xsl:if test="count($mediaflsh/data[@alias='umbracoFile']) &gt; 0">
 <a href="javascript:showonly('{concat('imgjq',position())}')">
<div id="divthumbnail">
<img class="ExcommKnap" src="{$mediaflsh/data[@alias='umbracoFile']} " width = "120px" height ="63px" >
</img>
</div>
   </a>
  </xsl:if>
 </xsl:if>
<!--IF flash END-->


</xsl:for-each>

<!--Repetitive JavaScript-->

</div>

</xsl:template>





<xsl:template name="insertFlash">
  <xsl:param name="finalFileURL"/>
  <xsl:param name="width"/>
  <xsl:param name="height"/>
  <xsl:param name="finalCssId"/>

  <div id="{$finalCssId}">
    <xsl:text> </xsl:text>
  </div>

  <script type="text/javascript" src="/scripts/swfobjectss.js">
    <xsl:text> </xsl:text>
  </script>

  <script type="text/javascript">
    <![CDATA[
    var flashvars = {};
    var params = {};
    var attributes = {};

    var path = "]]><xsl:value-of select="$finalFileURL" /><![CDATA[";
    var cssId = "]]><xsl:value-of select="$finalCssId" /><![CDATA[";
    var width = "]]><xsl:value-of select="$width" /><![CDATA[";
    var height = "]]><xsl:value-of select="$height" /><![CDATA[";

    swfobject.embedSWF( path, cssId, width, height, "9.0.0", false, flashvars, params, attributes);
    ]]>
  </script>

</xsl:template>





</xsl:stylesheet>
