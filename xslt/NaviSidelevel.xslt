<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt" 
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets ">

<xsl:output method="xml" omit-xml-declaration="yes" />

<xsl:param name="currentPage"/>

<xsl:template match="/">

<!-- The fun starts here -->
&nbsp;<br/>

<!---LEVEL 2 NAVI patents-->
<xsl:if test = "$currentPage/@level = 2">
<xsl:if test="$currentPage/@id = '1076'">
<xsl:variable name ="newProducts" select ="umbraco.library:GetXmlNodeById(1065)"/>
<xsl:if test="$newProducts/@id = '1065'">
<xsl:for-each select="$newProducts//node">	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
</xsl:for-each>
</xsl:if>
</xsl:if>
<xsl:if test="$currentPage/@id != '1076'">
<xsl:for-each select="$currentPage/node">
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
</xsl:for-each>
</xsl:if>
</xsl:if>

<!---LEVEL 3 NAVI patents-->
<xsl:if test = "$currentPage/@level = 3">
<xsl:if test="$currentPage/../@id = '1057'">
<xsl:for-each select="$currentPage/../node">
	<xsl:if test = "$currentPage/@id != ./@id">	
	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
	</xsl:if>
</xsl:for-each>
<br />


<xsl:if test="$currentPage/@id != '1072'">
<xsl:for-each select="$currentPage/node">	
	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
</xsl:for-each>
<br />
<xsl:for-each select="$currentPage/node/node">	
	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
</xsl:for-each>
</xsl:if>
</xsl:if>
<xsl:if test="$currentPage/../@id = '1065'">

<xsl:for-each select="$currentPage/../node">
	<xsl:if test = "$currentPage/@id != ./@id">	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	</xsl:if>
</xsl:for-each>
<br/>

<xsl:variable name ="newLisense" select ="umbraco.library:GetXmlNodeById(1076)"/>
<a href="{umbraco.library:NiceUrl(1076)}"><xsl:value-of select="$newLisense/@nodeName"/></a>
<br/>
<xsl:variable name ="newProjects" select ="umbraco.library:GetXmlNodeById(1072)"/>
<a href="{umbraco.library:NiceUrl(1072)}"><xsl:value-of select="$newProjects/@nodeName"/></a>

</xsl:if>

</xsl:if>


<!---LEVEL 4 NAVI patents-->
<xsl:if test = "$currentPage/@level = 4">
<xsl:if test="$currentPage/../@id != '1072'">
<xsl:for-each select="$currentPage/node">	
	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
</xsl:for-each>
</xsl:if>
<xsl:if test="$currentPage/../@id = '1072'">
<xsl:for-each select="$currentPage/..">
	<xsl:if test = "$currentPage/@id != ./@id">	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
	</xsl:if>
</xsl:for-each>
<br/>
<xsl:for-each select="$currentPage/node">
	<xsl:if test = "$currentPage/@id != ./@id">	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
	</xsl:if>
</xsl:for-each>

</xsl:if>


</xsl:if>


<!---LEVEL 5 NAVI patents-->
<xsl:if test = "$currentPage/@level = 5">
<xsl:if test="$currentPage/../../@id != '1072'">
<xsl:for-each select="$currentPage/..">	
	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
</xsl:for-each>
<br />

<xsl:for-each select="$currentPage/../node">
	<xsl:if test = "$currentPage/@id != ./@id">	
	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
	</xsl:if>
</xsl:for-each>
</xsl:if>

<xsl:if test="$currentPage/../../@id = '1072'">
<xsl:for-each select="$currentPage/../..">
	<xsl:if test = "$currentPage/@id != ./@id">	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
	</xsl:if>
</xsl:for-each>
<br/>
<xsl:for-each select="$currentPage/..">
	<xsl:if test = "$currentPage/@id != ./@id">	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
	</xsl:if>
</xsl:for-each>
<br/>

<xsl:for-each select="$currentPage/../node">
	<xsl:if test = "$currentPage/@id != ./@id">	
	
		<a href="{umbraco.library:NiceUrl(@id)}">
			<xsl:value-of select="@nodeName"/>
		</a><br/>
	
	</xsl:if>
</xsl:for-each>



</xsl:if>




</xsl:if>





</xsl:template>

</xsl:stylesheet>