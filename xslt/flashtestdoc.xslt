<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
        xmlns:msxml="urn:schemas-microsoft-com:xslt"
        xmlns:randomTools="http://www.umbraco.org/randomTools"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" 
	exclude-result-prefixes="msxml randomTools umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets ">

<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">

  <xsl:variable name="mediaFile" select="umbraco.library:GetMedia($currentPage/data [@alias='tFlashFile'], 'false')/data [@alias = 'umbracoFile']"/>
  <xsl:variable name="fileURL" select="/macro/fileURL"/>
  <xsl:variable name="width" select="$currentPage/data[@alias='tFlashWidth']"/>
  <xsl:variable name="height" select="$currentPage/data[@alias='tFlashHeight']"/>
  <xsl:variable name="cssId" select="/macro/cssId"/>
  <xsl:variable name="urlParameters" select="/macro/urlParameters"/>

<xsl:if test="$currentPage/data [@alias='tFlashFile'] != ''">
          
        <xsl:value-of select="umbraco.library:GetMedia($currentPage/data [@alias='tFlashFile'], 'false')/data [@alias = 'umbracoFile']" />
            
         </xsl:if>


<xsl:value-of select="$mediaFile" /><br/>
<xsl:value-of select="$width" /><br/>
<xsl:value-of select="$height" />


  <xsl:if test="$mediaFile != '' or $fileURL != ''">
    <xsl:call-template name="insertFlash">
      <xsl:with-param name="finalFileURL" select="$mediaFile"></xsl:with-param>
      <xsl:with-param name="width" select="$width"></xsl:with-param>
      <xsl:with-param name="height" select="$height"></xsl:with-param>
      <xsl:with-param name="finalCssId" select="$cssId"></xsl:with-param>
    </xsl:call-template>

  </xsl:if>

</xsl:template>

<xsl:template name="insertFlash">
  <xsl:param name="finalFileURL"/>
  <xsl:param name="width"/>
  <xsl:param name="height"/>
  <xsl:param name="finalCssId"/>

  <div id="{$finalCssId}">
    <xsl:text> </xsl:text>
  </div>

  <script type="text/javascript" src="/scripts/swfobjectss.js">
    <xsl:text> </xsl:text>
  </script>

  <script type="text/javascript">
    <![CDATA[
    var flashvars = {};
    var params = {};
    var attributes = {};

    var path = "]]><xsl:value-of select="$finalFileURL" /><![CDATA[";
    var cssId = "]]><xsl:value-of select="$finalCssId" /><![CDATA[";
    var width = "]]><xsl:value-of select="$width" /><![CDATA[";
    var height = "]]><xsl:value-of select="$height" /><![CDATA[";
	alert('Testingtesting')
    swfobject.embedSWF( path, cssId, width, height, "9.0.0", false, flashvars, params, attributes);
    ]]>
  </script>

</xsl:template>



</xsl:stylesheet>